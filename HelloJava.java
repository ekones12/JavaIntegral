import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class HelloJava {

	public static double func1 (int a,int b,int n)// this part algorithm for calcuating first integrate.. 
	{
		double Deltax = b-a;
		Deltax = Deltax / n;
		
		double sum = 0;
		for(double i=a+Deltax/2;i<b;i=i+Deltax){
			
		
			double x = (i*i)-i + 3;
			x = x * Deltax ;
			sum = sum + x;
					
			
		}
		return sum;
		}
		
	public static double func2 (int a,int b,int n)//this part algorithm for calcuating second integrate..
	{
		double Deltax = b-a;
		Deltax = Deltax / n;
		
		double sum = 0;
		for(double i=a+Deltax/2;i<b;i=i+Deltax){
			
		
			double x = (3*Math.sin(i)-4)*(3*Math.sin(i)-4);
			x = x * Deltax ;
			sum = sum + x;
					
			
			}
		return sum;
		}
	
	public static double func3 (double a,double b,double n)// this part algorithm for calcuating last function integrate..
	{
		double Deltax = b-a;
		Deltax = Deltax / n;
		
		double sum = 0;
		for(double i=a+Deltax/2;i<b;i=i+Deltax){
			
		
			double x = arcsin(i);
			x = x * Deltax ;
			sum = sum + x;
					
			
			}
		return sum;
		}
	
	public static double arcsin (double a)// this part calculatng for arcsin formul
	{
		a = Math.abs(a);
		double sum = 0;
		
		for(int i=0;i<30;i++){
			
			double x = Math.pow(-1,i)*HelloJava.fact(2*i);
			double t = Math.pow(4, i) * HelloJava.fact(i)*HelloJava.fact(i)*(2*i +1);
			x = x/t;
			x = x * Math.pow(a, 2*i + 1);
			sum = sum + x;
			
			}
		return sum;
		}
		
	public static double fact(double a)// this part calculate factorial for calculate arcsinh
	{
        int fact=1;
        for(int i=1; i<=a; i++){
            fact=fact*i;
        }
        return fact;
    }
	
	public static String armstrong(int t)// this part finding armstrong numbers
	{
		String armstrong = "0 ";

		double x= 1;
		x = x * Math.pow(10, t);
		for(int i=0;i<x;i++){
			int a = i%10;
			int b = (i/10)%10;
			int c = (i/100)%10;
			int number = i;
			int degree = 0;
			while (number>0){
				degree++;
				number/=10;
			}
			for(int j=0;j<=degree;j++){
			
			double z = Math.pow(a, j)+Math.pow(b, j)+Math.pow(c, j);
			
			if(z==i){
				armstrong += i + " " ;
				
				}
			
			}
		}
		return armstrong;
		}
	
	public static String write (List<String> commandlist)// this part writing outputs 
	{
	String write = "" ;
	for (int i=0;i<commandlist.size();i++){
		String[] a = commandlist.get(i).split(" ");
		if (a[0].equals("IntegrateReimann")){
			if (a[1].equals("Func1")){
				write= write + a[0]+ " " + a[1] + " " + a[2]+" " +a[3]+" " + a[4]+" " + "Result: " + func1(Integer.parseInt(a[2]),Integer.parseInt(a[3]),Integer.parseInt(a[4])) + "\n";
				
			}
			else if (a[1].equals("Func2")){
				write= write + a[0]+ " " + a[1] + " " + a[2]+" " +a[3]+" " + a[4]+" " + "Result: " + func2(Integer.parseInt(a[2]),Integer.parseInt(a[3]),Integer.parseInt(a[4])) + "\n";
			}
			else if (a[1].equals("Func3")){
				write= write + a[0]+ " " + a[1] + " " + a[2]+" " +a[3]+" " + a[4]+" " + "Result: " + func3(Double.parseDouble(a[2]),Double.parseDouble(a[3]),Double.parseDouble(a[4])) + "\n";
		
			}
		
		
		}
		else if (a[0].equals("Arcsinh")){
			write= write + a[0] + " " + a[1] + " " + "Result : " + arcsin(Double.parseDouble(a[1]))+ " \n";		
			}
		else if (a[0].equals("Armstrong")){
			write= write + a[0] + " " + a[1] + " " + "Result : " + armstrong(Integer.parseInt(a[1]))+ " \n";	
			
		}
	}
	
	return write;
	
	}
	
	public static List<String> Read(String file)// this part read file and write to list 
	{
 
			List<String> data = new ArrayList<String>();
			
			 try {
			 Scanner scanner = new Scanner(new File(file));
			 while(scanner.hasNextLine()){
			 String line = scanner.nextLine();
			 data.add(line);
			 }
			 scanner.close();
			 }
			 catch (FileNotFoundException ex) {
			 System.out.println("No File Found!");
	
			 }
			 return data;
			}

	public static void main (String [] args)// this is main function for all operations
	{
		List <String> command = Read(args[0]);
		System.out.println(write(command));
		
		
	
		
		
	


	
	
	
	
	
	}
}
